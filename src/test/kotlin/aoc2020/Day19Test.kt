package aoc2020

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

class Day19Test : StringSpec({
    "extract message rule" {
        extractMessageRule(mapOf(
            0 to "\"a\"",
        )).shouldBe("a")
        extractMessageRule(mapOf(
            0 to "\"b\"",
        )).shouldBe("b")
        extractMessageRule(mapOf(
            0 to "1 2",
            1 to "\"a\"",
            2 to "\"b\"",
        )).shouldBe("ab")
        extractMessageRule(mapOf(
            0 to "1 2",
            1 to "\"a\"",
            2 to "1 3 | 3 1",
            3 to "\"b\"",
        )).shouldBe("a(ab|ba)")
        extractMessageRule(mapOf(
            0 to "4 1 5",
            1 to "2 3 | 3 2",
            2 to "4 4 | 5 5",
            3 to "4 5 | 5 4",
            4 to "\"a\"",
            5 to "\"b\"",
        )).shouldBe("a((aa|bb)(ab|ba)|(ab|ba)(aa|bb))b")
    }
})
