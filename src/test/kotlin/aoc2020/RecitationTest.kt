package aoc2020

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

class RecitationTest : StringSpec({
    "recite 2020th" {
        recite(listOf(0,3,6), 2020).shouldBe(436)
        recite(listOf(1,3,2), 2020).shouldBe(1)
        recite(listOf(2,1,3), 2020).shouldBe(10)
        recite(listOf(1,2,3), 2020).shouldBe(27)
        recite(listOf(2,3,1), 2020).shouldBe(78)
        recite(listOf(3,2,1), 2020).shouldBe(438)
        recite(listOf(3,1,2), 2020).shouldBe(1836)
    }
    "recite 30000000th" {
        recite(listOf(0,3,6), 30000000).shouldBe(175594)
        recite(listOf(1,3,2), 30000000).shouldBe(2578)
        recite(listOf(2,1,3), 30000000).shouldBe(3544142)
        recite(listOf(1,2,3), 30000000).shouldBe(261214)
        recite(listOf(2,3,1), 30000000).shouldBe(6895259)
        recite(listOf(3,2,1), 30000000).shouldBe(18)
        recite(listOf(3,1,2), 30000000).shouldBe(362)
    }
})
