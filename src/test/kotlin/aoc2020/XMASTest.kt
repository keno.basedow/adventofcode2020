package aoc2020

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.booleans.shouldBeTrue
import io.kotest.matchers.shouldBe

class XMASTest : StringSpec({
    "preamble 5" {
        numberValid(10, listOf(1, 2, 3, 4, 5)).shouldBeFalse()
        numberValid(40, listOf(35, 20, 15, 25, 47)).shouldBeTrue()
        numberValid(62, listOf(20, 15, 25, 47, 40)).shouldBeTrue()
        numberValid(127, listOf(95, 102, 117, 150, 182)).shouldBeFalse()
    }
})
