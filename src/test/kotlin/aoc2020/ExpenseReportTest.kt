package aoc2020

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

class ExpenseReportTest : StringSpec({
    "zero expense 2 for empty report" {
        expense(emptyList<Int>()) shouldBe 0
    }
    "zero expense 2 for one element report" {
        expense(listOf(1)) shouldBe 0
    }
    "expense 2 of two numbers" {
        expense(listOf(1, 2)) shouldBe 0
        expense(listOf(2019, 1)) shouldBe 2019
        expense(listOf(1010, 1010)) shouldBe 1020100
    }
    "expense 2 of three numbers" {
        expense(listOf(2, 2018, 1)) shouldBe 4036
        expense(listOf(1, 2, 2018)) shouldBe 4036
    }
    "example expense 2 from day 1" {
        expense(listOf(1721, 979, 366, 299, 675, 1456)) shouldBe 514579
    }
    "expense 3 of three numbers" {
        expense3(listOf(1, 2, 2017)) shouldBe 4034
    }
    "example expense 3 from day 1" {
        expense3(listOf(1721, 979, 366, 299, 675, 1456)) shouldBe 241861950
    }
    "expense 3 with one distance in between" {
        expense3(listOf(1721, 979, 299, 366, 1456, 675)) shouldBe 241861950
    }
})
