package aoc2020

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

class PlaneTest : StringSpec({
    "calculate row" {
        calculateRow("FBFBBFF").shouldBe(44)
        calculateRow("BFFFBBF").shouldBe(70)
        calculateRow("FFFBBBF").shouldBe(14)
        calculateRow("BBFFBBF").shouldBe(102)
    }
    "calculate column" {
        calculateColumn("RLR").shouldBe(5)
        calculateColumn("RRR").shouldBe(7)
        calculateColumn("RLL").shouldBe(4)
    }
    "calculate seat id" {
        calculateSeatId("FBFBBFFRLR").shouldBe(357)
        calculateSeatId("BFFFBBFRRR").shouldBe(567)
        calculateSeatId("FFFBBBFRRR").shouldBe(119)
        calculateSeatId("BBFFBBFRLL").shouldBe(820)
    }
})
