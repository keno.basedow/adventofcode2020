package aoc2020

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

class LuggageProcessingTest : StringSpec({
    "extract luggage info" {
        extractLuggageInfo("light red bags contain 1 bright white bag, 2 muted yellow bags.").shouldBe("light red" to mapOf("bright white" to 1, "muted yellow" to 2))
        extractLuggageInfo("dark orange bags contain 3 bright white bags, 4 muted yellow bags.").shouldBe("dark orange" to mapOf("bright white" to 3, "muted yellow" to 4))
        extractLuggageInfo("bright white bags contain 1 shiny gold bag.").shouldBe("bright white" to mapOf("shiny gold" to 1))
        extractLuggageInfo("muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.").shouldBe("muted yellow" to mapOf("shiny gold" to 2, "faded blue" to 9))
        extractLuggageInfo("shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.").shouldBe("shiny gold" to mapOf("dark olive" to 1, "vibrant plum" to 2))
        extractLuggageInfo("dark olive bags contain 3 faded blue bags, 4 dotted black bags.").shouldBe("dark olive" to mapOf("faded blue" to 3, "dotted black" to 4))
        extractLuggageInfo("vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.").shouldBe("vibrant plum" to mapOf("faded blue" to 5, "dotted black" to 6))
        extractLuggageInfo("faded blue bags contain no other bags.").shouldBe("faded blue" to emptyMap())
        extractLuggageInfo("dotted black bags contain no other bags.").shouldBe("dotted black" to emptyMap())
    }
})
