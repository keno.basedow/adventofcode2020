package aoc2020

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

class ShuttleServiceTest : StringSpec({
    "departure time" {
        extractDepature("939").shouldBe(939)
    }
    "bus departures" {
        extractBusDepatures("7,13,x,x,59,x,31,19").shouldBe(listOf(7, 13, 59, 31, 19))
    }
    "minutes until bus departure" {
        minutesUntilBusDeparture(7, 939).shouldBe(6)
        minutesUntilBusDeparture(13, 939).shouldBe(10)
        minutesUntilBusDeparture(59, 939).shouldBe(5)
    }
    "bus distances" {
        extractBusDistances("7,13,x,x,59,x,31,19").shouldBe(mapOf(0 to 7, 1 to 13, 4 to 59, 6 to 31, 7 to 19))
        extractBusDistances("17,x,13,19").shouldBe(mapOf(0 to 17, 2 to 13, 3 to 19))
        extractBusDistances("67,7,59,61").shouldBe(mapOf(0 to 67, 1 to 7, 2 to 59, 3 to 61))
    }
    "match bus schedule" {
        timestampMatchBusSchedule(mapOf(0 to 17, 2 to 13, 3 to 19)).shouldBe(3417L)
        timestampMatchBusSchedule(mapOf(0 to 67, 1 to 7, 2 to 59, 3 to 61)).shouldBe(754018L)
        timestampMatchBusSchedule(mapOf(0 to 67, 2 to 7, 3 to 59, 4 to 61)).shouldBe(779210L)
        timestampMatchBusSchedule(mapOf(0 to 67, 1 to 7, 3 to 59, 4 to 61)).shouldBe(1261476L)
        timestampMatchBusSchedule(mapOf(0 to 1789, 1 to 37, 2 to 47, 3 to 1889)).shouldBe(1202161486L)
    }
})
