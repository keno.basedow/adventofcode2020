package aoc2020

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

class WaitingAreaTest : StringSpec({
    val layout = listOf(
        "#.LL.L#.##",
        "#LLLLLL.L#",
        "L.L.L..L..",
        "#LLL.LL.L#",
        "#.LL.LL.LL",
        "#.LLLL#.##",
        "..L.L.....",
        "#LLLLLLLL#",
        "#.LLLLLL.L",
        "#.#LLLL.##"
    )
    val layout2 = listOf(
        "#.##.L#.##",
        "#L###LL.L#",
        "L.#.#..#..",
        "#L##.##.L#",
        "#.##.LL.LL",
        "#.###L#.##",
        "..#.#.....",
        "#L######L#",
        "#.LL###L.L",
        "#.#L###.##"
    )

    "adjacent occupied" {
        countAdjacentOccupied(layout, 0, 0).shouldBe(1)
        countAdjacentOccupied(layout, 7, 5).shouldBe(2)
        countAdjacentOccupied(layout, 1, 8).shouldBe(4)
        countAdjacentOccupied(layout, 8, 8).shouldBe(3)
    }
    "modified seat" {
        modifiedSeat(layout2, 0, 0, ::countAdjacentOccupied, 4).shouldBe('#')
        modifiedSeat(layout2, 2, 1, ::countAdjacentOccupied, 4).shouldBe('L')
        modifiedSeat(layout, 2, 1, ::countAdjacentOccupied, 4).shouldBe('#')
        modifiedSeat(layout, 1, 1, ::countAdjacentOccupied, 4).shouldBe('L')
    }
    "modified layout" {
        modifiedLayout(layout, ::countAdjacentOccupied, 4).shouldBe(layout2)
    }
    "line of sight occupied" {
        countLineOfSightsOccupied(listOf(
            ".......#.",
            "...#.....",
            ".#.......",
            ".........",
            "..#L....#",
            "....#....",
            ".........",
            "#........",
            "...#....."
        ), 3, 4).shouldBe(8)
        
        countLineOfSightsOccupied(listOf(
            ".............",
            ".L.L.#.#.#.#.",
            "............."
        ), 1, 1).shouldBe(0)

        countLineOfSightsOccupied(listOf(
            ".##.##.",
            "#.#.#.#",
            "##...##",
            "...L...",
            "##...##",
            "#.#.#.#",
            ".##.##."
        ), 3, 3).shouldBe(0)
    }
})
