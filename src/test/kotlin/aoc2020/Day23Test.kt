package aoc2020

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.nulls.shouldBeNull
import io.kotest.matchers.shouldBe

class Day23Test : StringSpec({
    "generate" {
        Cup.generateCircle("389125467").toSequence().shouldBe("389125467")
    }
    "next" {
        Cup.generateCircle("389125467").next(3).toSequence().shouldBe("125467389")
    }
    "find" {
        Cup.generateCircle("389125467").find(2L)?.toSequence().shouldBe("254673891")
        Cup.generateCircle("254673").find(1L).shouldBeNull()

    }
    "rounds" {
        Cup.generateCircle("389125467").play().toSequence().shouldBe("289154673")
        Cup.generateCircle("289154673").play().toSequence().shouldBe("546789132")
        Cup.generateCircle("546789132").play().toSequence().shouldBe("891346725")
        Cup.generateCircle("891346725").play().toSequence().shouldBe("467913258")
    }
    "sequence without first" {
        Cup.generateCircle("837419265").find(1L)?.toSequence(ignoreFirst = true).shouldBe("92658374")
    }
    "play many rounds" {
        val start10 = Cup.generateCircle("389125467")
        val end10 = start10.playMany(10L)
        end10.find(1)?.toSequence(ignoreFirst = true).shouldBe("92658374")

        val start100 = Cup.generateCircle("389125467")
        val end100 = start100.playMany(100L)
        end100.find(1)?.toSequence(ignoreFirst = true).shouldBe("67384529")
    }
    "generate more than in sequence" {
        Cup.generateCircle("54321", max = 6L).toSequence().shouldBe("543216")
        Cup.generateCircle("54321", max = 9L).toSequence().shouldBe("543216789")
    }
})
