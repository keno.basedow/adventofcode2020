package aoc2020

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

class PortSystemTest : StringSpec({
    "mask value" {
        maskValue(0, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX1").shouldBe(1)
        maskValue(1, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX1").shouldBe(1)
        maskValue(1, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX0").shouldBe(0)
        maskValue(0, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX0").shouldBe(0)
        maskValue(11, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X").shouldBe(73)
        maskValue(101, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X").shouldBe(101)
        maskValue(0, "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X").shouldBe(64)
    }
    "mask address" {
        maskAddress(0L, "00000000000000000000000000000000000X").shouldBe(listOf(0L, 1L))
        maskAddress(0L, "0000000000000000000000000000000000X0").shouldBe(listOf(0L, 2L))
        maskAddress(0L, "000000000000000000000000000000000000").shouldBe(listOf(0L))
        maskAddress(1L, "000000000000000000000000000000000000").shouldBe(listOf(1L))
        maskAddress(0L, "000000000000000000000000000000000001").shouldBe(listOf(1L))
        maskAddress(42L, "000000000000000000000000000000X1001X").shouldBe(listOf(26L, 27L, 58L, 59L))
        maskAddress(26L, "00000000000000000000000000000000X0XX").shouldBe(listOf(16L, 17L, 18L, 19L, 24L, 25L, 26L, 27L))
    }
    "extract mask" {
        extractMask("mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X").shouldBe("XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X")
        extractMask("mem[8] = 11").shouldBe(null)
    }
    "extract write" {
        extractWrite("mem[8] = 11").shouldBe(Write(8, 11L))
        extractWrite("mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X").shouldBe(null)
    }
})
