package aoc2020

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

class AnswersTest : StringSpec({
    "count anyone" {
        countAnyone(listOf("abcx", "abcy", "abcz")).shouldBe(6)
        countAnyone(listOf("abc")).shouldBe(3)
        countAnyone(listOf("a", "b", "c")).shouldBe(3)
        countAnyone(listOf("ab", "ac")).shouldBe(3)
        countAnyone(listOf("a", "a", "a", "a")).shouldBe(1)
        countAnyone(listOf("b")).shouldBe(1)
    }
    "anyone answer sum" {
        answersSum(listOf(
            listOf("abc"),
            listOf("a", "b", "c"),
            listOf("ab", "ac"),
            listOf("a", "a", "a", "a"),
            listOf("b")
        ), ::countAnyone).shouldBe(11)
    }
    "count everyone" {
        countEveryone(listOf("abc")).shouldBe(3)
        countEveryone(listOf("a", "b", "c")).shouldBe(0)
        countEveryone(listOf("ab", "ac")).shouldBe(1)
        countEveryone(listOf("a", "a", "a", "a")).shouldBe(1)
        countEveryone(listOf("b")).shouldBe(1)
    }
    "everyone answer sum" {
        answersSum(listOf(
            listOf("abc"),
            listOf("a", "b", "c"),
            listOf("ab", "ac"),
            listOf("a", "a", "a", "a"),
            listOf("b")
        ), ::countEveryone).shouldBe(6)
    }
})
