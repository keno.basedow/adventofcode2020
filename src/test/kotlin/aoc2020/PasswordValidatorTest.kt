package aoc2020

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.booleans.shouldBeTrue
import io.kotest.matchers.shouldBe

class PasswordValidatorTest : StringSpec({
    "extract password" {
        extractPassword("1-3 a: abcde").shouldBe(Password(1, 3, 'a', "abcde"))
        extractPassword("1-3 b: cdefg").shouldBe(Password(1, 3, 'b', "cdefg"))
        extractPassword("2-9 c: ccccccccc").shouldBe(Password(2, 9, 'c', "ccccccccc"))
    }
    "validate password count" {
        validatePasswordCount(Password(1, 3, 'a', "abcde")).shouldBeTrue()
        validatePasswordCount(Password(1, 3, 'b', "cdefg")).shouldBeFalse()
        validatePasswordCount(Password(2, 9, 'c', "ccccccccc")).shouldBeTrue()
        validatePasswordCount(Password(2, 9, 'c', "cccccccccc")).shouldBeFalse()
        validatePasswordCount(Password(2, 9, 'c', "abcde")).shouldBeFalse()
    }
    "validate password positions" {
        validatePasswordPositions(Password(1, 3, 'a', "abcde")).shouldBeTrue()
        validatePasswordPositions(Password(1, 3, 'b', "cdefg")).shouldBeFalse()
    }
})
