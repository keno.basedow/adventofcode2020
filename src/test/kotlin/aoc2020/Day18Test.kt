package aoc2020

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

class Day18Test : StringSpec({
    "terms" {
        solveTerm("0").shouldBe(0L)
        solveTerm("1").shouldBe(1L)
        solveTerm("1 + 2").shouldBe(3L)
        solveTerm("3 * 3").shouldBe(9L)
        solveTerm("1 + 2 * 3").shouldBe(9L)
        solveTerm("1 + 2 * 3 + 4 * 5 + 6").shouldBe(71L)
        solveTerm("1 + (2 * 3)").shouldBe(7L)
        solveTerm("(4 * (5 + 6))").shouldBe(44L)
        solveTerm("1 + (2 * 3) + (4 * (5 + 6))").shouldBe(51L)
        solveTerm("2 * 3 + (4 * 5)").shouldBe(26L)
        solveTerm("5 + (8 * 3 + 9 + 3 * 4 * 3)").shouldBe(437L)
        solveTerm("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))").shouldBe(12240L)
        solveTerm("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2").shouldBe(13632L)
    }
    "terms 2" {
        solveTerm2("1 + 2 * 3 + 4 * 5 + 6").shouldBe(231L)
        solveTerm2("1 + (2 * 3) + (4 * (5 + 6))").shouldBe(51L)
        solveTerm2("2 * 3 + (4 * 5)").shouldBe(46L)
        solveTerm2("5 + (8 * 3 + 9 + 3 * 4 * 3)").shouldBe(1445L)
        solveTerm2("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))").shouldBe(669060L)
        solveTerm2("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2").shouldBe(23340L)
    }
})
