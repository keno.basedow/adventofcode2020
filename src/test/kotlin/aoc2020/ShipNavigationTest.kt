package aoc2020

import io.kotest.assertions.throwables.shouldThrowAny
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

class ShipNavigationTest : StringSpec({
    "north" {
        north(Ship(0, 0, 0), 3).shouldBe(Ship(3, 0, 0))
    }
    "south" {
        south(Ship(0, 0, 0), 5).shouldBe(Ship(-5, 0, 0))
    }
    "east" {
        east(Ship(0, 0, 0), 2).shouldBe(Ship(0, 2, 0))
    }
    "west" {
        west(Ship(0, 0, 0), 4).shouldBe(Ship(0, -4, 0))
    }
    "forward" {
        forward(Ship(0, 0, 0), 1).shouldBe(Ship(1, 0, 0))
        forward(Ship(0, 0, 90), 6).shouldBe(Ship(0, 6, 90))
        forward(Ship(0, 0, 180), 7).shouldBe(Ship(-7, 0, 180))
        forward(Ship(0, 0, 270), 8).shouldBe(Ship(0, -8, 270))
        forward(Ship(0, 2, 90), 4).shouldBe(Ship(0, 6, 90))
    }
    "right" {
        right(Ship(0, 0, 0), 90).shouldBe(Ship(0, 0, 90))
        right(Ship(0, 0, 270), 180).shouldBe(Ship(0, 0, 90))
        shouldThrowAny { right(Ship(0, 0, 0), 80) }
        shouldThrowAny { right(Ship(0, 0, 0), -90) }
    }
    "left" {
        left(Ship(0, 0, 0), 90).shouldBe(Ship(0, 0, 270))
        left(Ship(0, 0, 270), 180).shouldBe(Ship(0, 0, 90))
        shouldThrowAny { left(Ship(0, 0, 0), 25) }
        shouldThrowAny { left(Ship(0, 0, 0), 0) }
    }
    "extract action" {
        shouldThrowAny { extractAction("NA") }
        shouldThrowAny { extractAction("O1") }
        extractAction("N3").shouldBe(Action(::north, 3))
        extractAction("S5").shouldBe(Action(::south, 5))
        extractAction("E4").shouldBe(Action(::east, 4))
        extractAction("W7").shouldBe(Action(::west, 7))
        extractAction("F10").shouldBe(Action(::forward, 10))
        extractAction("R90").shouldBe(Action(::right, 90))
        extractAction("L180").shouldBe(Action(::left, 180))
    }
    "waypoint forward" {
        wpForward(Ship(0, 0, 0, 1, 10), 10).shouldBe(Ship(10, 100, 0, 1, 10))
        wpForward(Ship(10, 100, 0, 4, 10), 7).shouldBe(Ship(38, 170, 0, 4, 10))
    }
    "waypoint right" {
        wpRight(Ship(38, 170, 0, 4, 10), 90).shouldBe(Ship(38, 170, 0, -10, 4))
        wpRight(Ship(38, 170, 0, -10, 4), 90).shouldBe(Ship(38, 170, 0, -4, -10))
        wpRight(Ship(38, 170, 0, 4, 10), 180).shouldBe(Ship(38, 170, 0, -4, -10))
    }
    "waypoint left" {
        wpLeft(Ship(38, 170, 0, -4, -10), 90).shouldBe(Ship(38, 170, 0, -10, 4))
        wpLeft(Ship(38, 170, 0, -10, 4), 90).shouldBe(Ship(38, 170, 0, 4, 10))
        wpLeft(Ship(38, 170, 0, -4, -10), 180).shouldBe(Ship(38, 170, 0, 4, 10))
    }
})
