package aoc2020

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

class Day24Test : StringSpec({
    "extract grid directions" {
        extractGridDirections("esenee").shouldBe(listOf("e", "se", "ne", "e"))
        extractGridDirections("esew").shouldBe(listOf("e", "se", "w"))
        extractGridDirections("nwwswee").shouldBe(listOf("nw", "w", "sw", "e", "e"))
        extractGridDirections("sesenwnenenewseeswwswswwnenewsewsw")
            .shouldBe(listOf("se", "se", "nw", "ne", "ne", "ne", "w", "se", "e", "sw", "w", "sw", "sw", "w", "ne", "ne", "w", "se", "w", "sw"))
    }
    "move" {
        move(listOf("w")).shouldBe(Coord(x = -1, y = 1))
        move(listOf("nw")).shouldBe(Coord(y = 1, z = -1))
        move(listOf("ne")).shouldBe(Coord(x = 1, z = -1))
        move(listOf("e")).shouldBe(Coord(x = 1, y = -1))
        move(listOf("se")).shouldBe(Coord(y = -1, z = 1))
        move(listOf("sw")).shouldBe(Coord(x = -1, z = 1))
        move(listOf("e", "se", "w")).shouldBe(Coord(y = -1, z = 1))
        move(listOf("nw", "w", "sw", "e", "e")).shouldBe(Coord())
    }
})
