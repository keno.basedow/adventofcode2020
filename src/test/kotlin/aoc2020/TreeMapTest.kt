package aoc2020

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.booleans.shouldBeTrue
import io.kotest.matchers.shouldBe

class TreeMapTest : StringSpec({
    val treeMap = TreeMap(
        listOf(
            "..##.......",
            "#...#...#..",
            ".#....#..#.",
            "..#.#...#.#",
            ".#...##..#.",
            "..#.##.....",
            ".#.#.#....#",
            ".#........#",
            "#.##...#...",
            "#...##....#",
            ".#..#...#.#"
        ).map { it.map { c -> c == '#' } }
    )

    "is tree in grid" {
        treeMap.isTree(0, 0).shouldBeFalse()
        treeMap.isTree(1, 0).shouldBeFalse()
        treeMap.isTree(2, 0).shouldBeTrue()
        treeMap.isTree(2, 5).shouldBeTrue()
        treeMap.isTree(3, 5).shouldBeFalse()
    }

    "is tree extended grid" {
        treeMap.isTree(11, 0).shouldBeFalse()
        treeMap.isTree(13, 0).shouldBeTrue()
        treeMap.isTree(27, 10).shouldBeFalse()
        treeMap.isTree(26, 10).shouldBeTrue()
    }

    "count trees" {
        treeMap.countTrees(3, 1).shouldBe(7)
        treeMap.countTrees(2, 2).shouldBe(1)
        treeMap.countTrees(4, 1).shouldBe(2)
        treeMap.countTrees(2, 1).shouldBe(1)
        treeMap.countTrees(0, 1).shouldBe(3)
    }

    "count trees slope examples B" {
        treeMap.countTrees(1, 1).shouldBe(2)
        treeMap.countTrees(5, 1).shouldBe(3)
        treeMap.countTrees(7, 1).shouldBe(4)
        treeMap.countTrees(1, 2).shouldBe(2)
    }
})
