package aoc2020

import java.io.File

fun main() {
    val answers = File("input/day6.txt").readText()
        .split(Regex("""\r?\n\s*\r?\n"""))
        .filter { it.isNotBlank() }
        .map { s ->
            s.split(Regex("""\r?\n"""))
                .filter { it.isNotBlank() }
        }

    println("Day 6 A answer is ${answersSum(answers, ::countAnyone)}")
    println("Day 6 B answer is ${answersSum(answers, ::countEveryone)}")
}