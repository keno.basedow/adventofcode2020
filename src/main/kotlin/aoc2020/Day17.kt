package aoc2020

import java.io.File

data class Cube(val x: Int, val y: Int, val z: Int = 0, val w: Int = 0)

fun neighbors(cube: Cube) =
    (-1..1).flatMap { z ->
        (-1..1).flatMap { y ->
            (-1..1).map { x ->
                Cube(cube.x + x, cube.y + y, cube.z + z)
            }
        }
    }.minus(cube).toSet()

fun neighbors4D(cube: Cube) =
    (-1..1).flatMap { w ->
        (-1..1).flatMap { z ->
            (-1..1).flatMap { y ->
                (-1..1).map { x ->
                    Cube(cube.x + x, cube.y + y, cube.z + z, cube.w + w)
                }
            }
        }
    }.minus(cube).toSet()

fun activeNeighbors(cube: Cube, activeCubes: Set<Cube>) =
    activeCubes.intersect(neighbors(cube))

fun activeNeighbors4D(cube: Cube, activeCubes: Set<Cube>) =
    activeCubes.intersect(neighbors4D(cube))

fun changedCube(cube: Cube, activeCubes: Set<Cube>) =
    if (cube in activeCubes)
        if (activeNeighbors(cube, activeCubes).size in 2..3) cube else null
    else
        if (activeNeighbors(cube, activeCubes).size == 3) cube else null

fun changedCube4D(cube: Cube, activeCubes: Set<Cube>) =
    if (cube in activeCubes)
        if (activeNeighbors4D(cube, activeCubes).size in 2..3) cube else null
    else
        if (activeNeighbors4D(cube, activeCubes).size == 3) cube else null

fun changedCubes(activeCubes: Set<Cube>) =
    (activeCubes.minOf { it.z } - 1 .. activeCubes.maxOf { it.z } + 1).flatMap { z ->
        (activeCubes.minOf { it.y } - 1 .. activeCubes.maxOf { it.y } + 1).flatMap { y ->
            (activeCubes.minOf { it.x } - 1 .. activeCubes.maxOf { it.x } + 1).map { x ->
                changedCube(Cube(x, y, z), activeCubes)
            }
        }
    }.filterNotNull().toSet()

fun changedCubes4D(activeCubes: Set<Cube>) =
    (activeCubes.minOf { it.w } - 1 .. activeCubes.maxOf { it.w } + 1).flatMap { w ->
        (activeCubes.minOf { it.z } - 1 .. activeCubes.maxOf { it.z } + 1).flatMap { z ->
            (activeCubes.minOf { it.y } - 1..activeCubes.maxOf { it.y } + 1).flatMap { y ->
                (activeCubes.minOf { it.x } - 1..activeCubes.maxOf { it.x } + 1).map { x ->
                    changedCube4D(Cube(x, y, z, w), activeCubes)
                }
            }
        }
    }.filterNotNull().toSet()

fun printCubes(activeCubes: Set<Cube>) {
    for (w in activeCubes.minOf { it.w } .. activeCubes.maxOf { it.w } ) {
        for (z in activeCubes.minOf { it.z }..activeCubes.maxOf { it.z }) {
            println("z=$z, w=$w")
            for (y in activeCubes.minOf { it.y }..activeCubes.maxOf { it.y }) {
                for (x in activeCubes.minOf { it.x }..activeCubes.maxOf { it.x }) {
                    print(if (Cube(x, y, z) in activeCubes) '#' else '.')
                }
                println()
            }
        }
    }
}

fun main() {
    val activeCubes = File("input/day17.txt")
        .useLines { it.toList() }
        .flatMapIndexed { y, xList -> xList.mapIndexedNotNull { x, c ->
            when (c) {
                '#' -> Cube(x, y)
                else -> null
            } } }
        .toSet()

    printCubes(activeCubes)

    var cubes = activeCubes
    (1..6).forEach { _ -> cubes = changedCubes(cubes) }

//    printCubes(changedCubes4D(activeCubes))

    var cubes4D = activeCubes
    (1..6).forEach { _ -> cubes4D = changedCubes4D(cubes4D) }

    println("Day 17 A answer is ${cubes.size}")
    println("Day 17 B answer is ${cubes4D.size}")

}
