package aoc2020

fun calculateRow(rowStr: String): Int {
    if (rowStr.count() != 7) return 0
    return rowStr
        .replace('F', '0')
        .replace('B', '1')
        .toInt(2)
}

fun calculateColumn(colStr: String): Int {
    if (colStr.count() != 3) return 0
    return colStr
        .replace('L', '0')
        .replace('R', '1')
        .toInt(2)
}

fun calculateSeatId(seatStr: String): Int {
    if (seatStr.count() != 10) return 0
    return calculateRow(seatStr.take(7)) * 8 + calculateColumn(seatStr.drop(7))
}