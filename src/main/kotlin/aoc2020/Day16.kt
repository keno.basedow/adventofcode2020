package aoc2020

import java.io.File
import java.lang.IllegalArgumentException

fun extractNumber(match: MatchResult?, index: Int) = match?.groups?.elementAtOrNull(index)?.value?.toIntOrNull() ?: 0
fun extractRange(match: MatchResult?, index: Int) = IntRange(extractNumber(match, index), extractNumber(match, index + 1))

fun extractRule(line: String): Pair<String, List<IntRange>> {
    val match = Regex("""([^:]+):\s*(\d+)-(\d+)\s+or\s+(\d+)-(\d+)""").matchEntire(line)
    val field = match?.groups?.elementAtOrNull(1)?.value ?: throw IllegalArgumentException()
    val ranges = listOf(extractRange(match,2), extractRange(match, 4))
    return field to ranges
}

fun invalid(rules: List<IntRange>, value: Int) =
    rules.none { range -> value in range }

fun ticketValid(rules: Map<String, List<IntRange>>, values: List<Int>) =
    values.none { invalid(rules.values.flatten(), it) }

fun valuesValid(rules: List<IntRange>, values: List<Int>) =
    values.none { invalid(rules, it) }

fun main() {
    val parts = File("input/day16.txt").readText()
        .split(Regex("""\r?\n\s*\r?\n"""))
        .filter { it.isNotBlank() }

    val rules = parts[0]
        .split(Regex("""\r?\n"""))
        .associate { extractRule(it) }

    val yourTicket = parts[1]
        .split(Regex("""\r?\n""")).last()
        .split(',')
        .mapNotNull { it.toIntOrNull() }
    val nearbyTickets = parts[2]
        .split(Regex("""\r?\n"""))
        .drop(1)
        .filter { it.isNotBlank() }
        .map { ticket ->
            ticket.split(',')
                .mapNotNull { it.toIntOrNull() }
        }

//    println(rules)
//    println(yourTicket)
//    println(nearbyTickets)

    val day16A = nearbyTickets.flatten().filter { invalid(rules.values.flatten(), it) }.sum()
    println("Day 16 A answer is $day16A")

    val validTickets = (nearbyTickets + listOf(yourTicket)).filter { ticketValid(rules, it) }
    val fieldValuesList = mutableListOf<List<Int>>()
    for (i in validTickets.first().indices) {
        fieldValuesList.add(validTickets.map { it[i] })
    }
//    println(fieldValuesList)

    val rulesList = rules.toList()
    val nameToIndex = mutableMapOf<String, Int>()
    while (nameToIndex.size < yourTicket.size) {
        for ((index, fieldValues) in fieldValuesList.withIndex()) {
            if (index in nameToIndex.values) continue
            val matchingRule = rulesList.filter { it.first !in nameToIndex }
                .singleOrNull { valuesValid(it.second, fieldValues) } ?: continue
            val name = matchingRule.first
//            println("$name: $fieldValues")
            nameToIndex[name] = index
        }
    }
//    println(nameToIndex)
//    println(nameToIndex.filter { it.key.startsWith("departure ") })
    val day16B = nameToIndex.toList()
        .filter { it.first.startsWith("departure ") }
        .map { it.second }
        .fold(1L) { acc, index -> acc * yourTicket[index] }
    println("Day 16 B answer is $day16B")
}