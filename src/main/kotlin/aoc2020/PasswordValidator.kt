package aoc2020

data class Password(
    val policy1: Int,
    val policy2: Int,
    val char: Char,
    val pass: String
)

fun extractPassword(line: String): Password? {
    val re = Regex("""(\d+)-(\d+)\s+(\w):\s+(\w+)""")
    val match = re.matchEntire(line) ?: return null
    with(match) {
        val policy1 = match.groups[1]?.value?.toIntOrNull() ?: return null
        val policy2 = match.groups[2]?.value?.toIntOrNull() ?: return null
        val char = match.groups[3]?.value?.elementAtOrNull(0) ?: return null
        val pass = match.groups[4]?.value ?: return null
        return Password(policy1, policy2, char, pass)
    }
}

fun validatePasswordCount(password: Password): Boolean {
    val count = password.pass.count { it == password.char }
    return password.policy1 <= count && count <= password.policy2
}

fun validatePasswordPositions(password: Password): Boolean {
    val char1Found = password.pass[password.policy1 - 1] == password.char
    val char2Found = password.pass[password.policy2 - 1] == password.char
    return char1Found xor char2Found
}
