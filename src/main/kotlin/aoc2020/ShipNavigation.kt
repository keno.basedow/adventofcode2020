package aoc2020

import java.lang.IllegalArgumentException

data class Ship(
    val ns: Int = 0,
    val ew: Int = 0,
    val dir: Int = 90,
    val wpns: Int = 1,
    val wpew: Int = 10
)

data class Action(
    val command: (Ship, Int) -> Ship,
    val value: Int
)

fun north(ship: Ship, value: Int) = ship.copy(ns = ship.ns + value)
fun south(ship: Ship, value: Int) = ship.copy(ns = ship.ns - value)
fun east(ship: Ship, value: Int) = ship.copy(ew = ship.ew + value)
fun west(ship: Ship, value: Int) = ship.copy(ew = ship.ew - value)

fun checkDegree(value: Int, code: () -> Ship): Ship {
    if (value > 0 && value % 90 == 0)
        return code()
    else
        throw IllegalArgumentException("Invalid degree $value")
}

fun right(ship: Ship, value: Int) = checkDegree(value) { ship.copy(dir = (ship.dir + value) % 360) }
fun left(ship: Ship, value: Int) = checkDegree(value) { ship.copy(dir = (ship.dir - value + 360) % 360) }

fun forward(ship: Ship, value: Int) = when (ship.dir) {
    0 -> north(ship, value)
    90 -> east(ship, value)
    180 -> south(ship, value)
    270 -> west(ship, value)
    else -> ship
}

private fun extractCommand(line: String) = line.take(1)
private fun extractValue(line: String) = line.drop(1).toInt()

fun extractAction(line: String) = when (extractCommand(line)) {
    "N" -> Action(::north, extractValue(line))
    "S" -> Action(::south, extractValue(line))
    "E" -> Action(::east, extractValue(line))
    "W" -> Action(::west, extractValue(line))
    "R" -> Action(::right, extractValue(line))
    "L" -> Action(::left, extractValue(line))
    "F" -> Action(::forward, extractValue(line))
    else -> throw IllegalArgumentException("Invalid command ${extractCommand(line)}")
}

fun wpNorth(ship: Ship, value: Int) = ship.copy(wpns = ship.wpns + value)
fun wpSouth(ship: Ship, value: Int) = ship.copy(wpns = ship.wpns - value)
fun wpEast(ship: Ship, value: Int) = ship.copy(wpew = ship.wpew + value)
fun wpWest(ship: Ship, value: Int) = ship.copy(wpew = ship.wpew - value)

fun wpRight(ship: Ship, value: Int) = (0 until value step 90)
    .fold(ship) { s, _ -> s.copy(wpns = -s.wpew, wpew = s.wpns) }

fun wpLeft(ship: Ship, value: Int) = (0 until value step 90)
    .fold(ship) { s, _ -> s.copy(wpns = s.wpew, wpew = -s.wpns) }

fun wpForward(ship: Ship, value: Int) = ship.copy(ns = ship.ns + ship.wpns * value, ew = ship.ew + ship.wpew * value)

fun extractWaypointAction(line: String) = when (extractCommand(line)) {
    "N" -> Action(::wpNorth, extractValue(line))
    "S" -> Action(::wpSouth, extractValue(line))
    "E" -> Action(::wpEast, extractValue(line))
    "W" -> Action(::wpWest, extractValue(line))
    "R" -> Action(::wpRight, extractValue(line))
    "L" -> Action(::wpLeft, extractValue(line))
    "F" -> Action(::wpForward, extractValue(line))
    else -> throw IllegalArgumentException("Invalid command ${extractCommand(line)}")
}
