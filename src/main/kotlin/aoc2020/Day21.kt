package aoc2020

import java.io.File
import java.lang.IllegalArgumentException

data class Food(
    val ingredients: Set<String>,
    val allergens: Set<String>,
)

fun main() {
    val food = File("input/day21.txt")
        .useLines { it.toList() }
        .map { line ->
            Regex("""([^(]+)\s+\(contains\s+([^)]+)\)""").matchEntire(line)?.groupValues?.let {
                Food(Regex("""\s+""").split(it[1]).toSet(),
                    Regex(""",\s+""").split(it[2]).toSet())
            } ?: throw IllegalArgumentException("invalid food: $line")
        }
//    println(food)

    val allergens = food.flatMap { it.allergens }.toSet()
//    println(allergens)

    var ingredientListByAllergen = allergens.map { allergen -> allergen to
            food.filter { allergen in it.allergens }
                .map { it.ingredients }
                .reduce { acc, ingredients -> acc.intersect(ingredients) }
    }.toMap()
//    println(ingredientListByAllergen)
    val ingredientsWithAllergens = ingredientListByAllergen.flatMap { it.value }.toSet()
//    println(ingredientsWithAllergens)

    val ingrediantsWithoutAllergens = food.flatMap { it.ingredients - ingredientsWithAllergens }
//    println(ingrediantsWithoutAllergens)

    val day21A = ingrediantsWithoutAllergens.size
    println("Day 21 A answer is $day21A")

    var ingredientByAllergen = mutableListOf<Pair<String, String>>()
    while (ingredientListByAllergen.isNotEmpty()) {
        val allergen = ingredientListByAllergen.filter { it.value.size == 1 }.map { it.key }.first()
        val ingredient = ingredientListByAllergen[allergen]?.single() ?: ""
        ingredientByAllergen.add(allergen to ingredient)
        ingredientListByAllergen = ingredientListByAllergen
            .filter { it.key != allergen }
            .map { it.key to (it.value - ingredient) }
            .toMap()
    }
//    println(ingredientByAllergen)

    val day21B = ingredientByAllergen.sortedWith(compareBy { it.first }).map { it.second }.joinToString(",")
    println("Day 21 B answer is $day21B")
}