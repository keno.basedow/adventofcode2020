package aoc2020

import java.io.File

data class Bag(
    val name: String,
    val count: Int = 0,
    val containedIn: MutableSet<Bag> = mutableSetOf()
)

fun main() {
    val bagInfo = File("input/day7.txt")
        .useLines { it.toList() }
        .mapNotNull { extractLuggageInfo(it) }
        .toMap()

    val containedIn = mutableMapOf<String, Bag>()
    bagInfo.forEach { info ->
            info.value.keys.forEach {
                containedIn.getOrPut(it) { Bag(it) }.containedIn.add(containedIn.getOrPut(info.key) { Bag(info.key) })
            }
        }
    val containingBags = getContainingBags(containedIn.getOrDefault("shiny gold", Bag("shiny gold")))
    val bagCount = containingBags.count() - 1
    println("Day 7 A answer is $bagCount")

    println("Day 7 B answer is ${calculateBagCount(bagInfo, "shiny gold")}")
}