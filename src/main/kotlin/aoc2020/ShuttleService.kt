package aoc2020

import java.lang.IllegalArgumentException

fun extractDepature(line: String) = line.toInt()

fun extractBusDepatures(line: String) =
    line.split(',')
        .filter { it != "x" }
        .mapNotNull { it.toIntOrNull() }

fun minutesUntilBusDeparture(busInterval: Int, estimatedTime: Int) =
    (((estimatedTime / busInterval) + 1) * busInterval) - estimatedTime

fun extractBusDistances(line: String) =
    line.split(',')
        .map { it.toIntOrNull() ?: 0 }
        .mapIndexed { distance, id -> distance to id }
        .filter { it.second > 0 }
        .toMap()

fun timestampMatchBusSchedule(schedule: Map<Int, Int>): Long {
    val (offset, refId) = schedule.maxByOrNull { it.value } ?: throw IllegalArgumentException("no bus on timestamp zero")
    val remainingSchedule = (schedule - offset)
    var sequence = generateSequence(0L) {
        it + refId
    }
    for ((distance, id) in remainingSchedule) {
        sequence = sequence.filter { ((it - offset + distance) % id) == 0L }
    }
    return sequence.first() - offset
}