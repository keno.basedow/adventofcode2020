package aoc2020

import java.io.File

fun main() {
    val report = File("input/day1.txt").readText()
        .split(Regex("\\s+"))
        .filter { it.isNotBlank() }
        .map { it.toInt() }

    println("Day 1 A answer is ${expense(report)}")
    println("Day 1 B answer is ${expense3(report)}")
}
