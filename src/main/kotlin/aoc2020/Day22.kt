package aoc2020

import java.io.File

fun playGame(deck1: List<Int>, deck2: List<Int>): Pair<Int, List<Int>> {
    var gameDeck1 = deck1
    var gameDeck2 = deck2
    val decks1Seen = mutableListOf<List<Int>>()
    val decks2Seen = mutableListOf<List<Int>>()
    while (gameDeck1.isNotEmpty()
        && gameDeck2.isNotEmpty()
        && gameDeck1 !in decks1Seen
        && gameDeck2 !in decks2Seen)
    {
        decks1Seen.add(gameDeck1)
        decks2Seen.add(gameDeck2)
//        println("$gameDeck1 | $gameDeck2")
        val card1 = gameDeck1.first()
        gameDeck1 = gameDeck1.drop(1)
        val card2 = gameDeck2.first()
        gameDeck2 = gameDeck2.drop(1)
        if (card1 > card2) {
            gameDeck1 = gameDeck1 + card1 + card2
        } else if (card2 > card1) {
            gameDeck2 = gameDeck2 + card2 + card1
        }
    }
    if (gameDeck1.isNotEmpty() || gameDeck1 in decks1Seen || gameDeck2 in decks2Seen)
        return Pair(1, gameDeck1)
    else
        return Pair(2, gameDeck2)
}

fun playRecursiveGame(deck1: List<Int>, deck2: List<Int>): Pair<Int, List<Int>> {
    var gameDeck1 = deck1
    var gameDeck2 = deck2
    val decks1Seen = mutableListOf<List<Int>>()
    val decks2Seen = mutableListOf<List<Int>>()
    while (gameDeck1.isNotEmpty()
        && gameDeck2.isNotEmpty()
        && gameDeck1 !in decks1Seen
        && gameDeck2 !in decks2Seen)
    {
        decks1Seen.add(gameDeck1)
        decks2Seen.add(gameDeck2)
//        println("$gameDeck1 | $gameDeck2")
        val card1 = gameDeck1.first()
        gameDeck1 = gameDeck1.drop(1)
        val card2 = gameDeck2.first()
        gameDeck2 = gameDeck2.drop(1)
        if (gameDeck1.size >= card1 && gameDeck2.size >= card2) {
            val (winner, _) = playRecursiveGame(gameDeck1.take(card1), gameDeck2.take(card2))
            if (winner == 1) {
                gameDeck1 = gameDeck1 + card1 + card2
            } else {
                gameDeck2 = gameDeck2 + card2 + card1
            }
        } else if (card1 > card2) {
            gameDeck1 = gameDeck1 + card1 + card2
        } else if (card2 > card1) {
            gameDeck2 = gameDeck2 + card2 + card1
        }
    }
    if (gameDeck1.isNotEmpty() || gameDeck1 in decks1Seen || gameDeck2 in decks2Seen)
        return Pair(1, gameDeck1)
    else
        return Pair(2, gameDeck2)
}

fun score(deck: List<Int>) =
    deck.mapIndexed { index, card -> (deck.size - index) * card }.sum()

fun main() {
    val decks = File("input/day22.txt").readText()
        .split(Regex("""\r?\n\s*\r?\n"""))
        .filter { it.isNotBlank() }
        .map { lines -> lines.split(Regex("""\r?\n""")).filter { it.isNotBlank() }.mapNotNull { it.toIntOrNull() } }

    val (_, winDeckA) = playGame(decks[0], decks[1])
    println("Day 22 A answer is ${score(winDeckA)}")

    val (_, winDeckB) = playRecursiveGame(decks[0], decks[1])
    println("Day 22 B answer is ${score(winDeckB)}")
}