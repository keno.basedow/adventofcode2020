package aoc2020

import java.io.File
import java.lang.IllegalArgumentException

fun main() {
    val terms = File("input/day18.txt")
        .useLines { it.toList() }

    val day18A = terms.sumOf { solveTerm(it) }
    println("Day 18 A answer is $day18A")

    val day18B = terms.sumOf { solveTerm2(it) }
    println("Day 18 B answer is $day18B")
}

fun solveTerm(term: String): Long {
    term.toLongOrNull()?.let {
        return it
    }
    val reBrackets = Regex("""\(([^()]+)\)""")
    if (reBrackets.containsMatchIn(term)) {
        return solveTerm(reBrackets.replace(term) {
            solveTerm(it.groupValues[1]).toString()
        })
    }
    val reOperator = Regex("""^(\d+)\s*([+*])\s*(\d+)""")
    if (reOperator.containsMatchIn(term)) {
        return solveTerm(reOperator.replace(term) {
            when (val operator = it.groupValues[2]) {
                "+" -> it.long(1) + it.long(3)
                "*" -> it.long(1) * it.long(3)
                else -> throw IllegalArgumentException("invalid operator '$operator'")
            }.toString()
        })
    }
    throw IllegalArgumentException("Term '$term' could not be resolved")
}

fun solveTerm2(term: String): Long {
    term.toLongOrNull()?.let {
        return it
    }
    val reBrackets = Regex("""\(([^()]+)\)""")
    if (reBrackets.containsMatchIn(term)) {
        return solveTerm2(reBrackets.replace(term) {
            solveTerm2(it.groupValues[1]).toString()
        })
    }
    val rePlus = Regex("""(\d+)\s*\+\s*(\d+)""")
    if (rePlus.containsMatchIn(term)) {
        return solveTerm2(rePlus.replace(term) { (it.long(1) + it.long(2)).toString() })
    }
    val reMul = Regex("""(\d+)\s*\*\s*(\d+)""")
    if (reMul.containsMatchIn(term)) {
        return solveTerm2(reMul.replace(term) { (it.long(1) * it.long(2)).toString() })
    }
    throw IllegalArgumentException("Term '$term' could not be resolved")
}

fun MatchResult.long(index: Int) = this.groupValues[index].toLong()