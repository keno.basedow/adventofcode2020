package aoc2020

fun transform(subjectNumber: Int, loopSize: Int): Int {
    return subjectNumber.toBigInteger().modPow(loopSize.toBigInteger(), 20201227.toBigInteger()).toInt()
//    var value = 1
//    (1..loopSize).forEach {
//        value = (value * subjectNumber) % 20201227
//    }
//    println(value)
//    return value
}

fun findLoopSizeFor(key: Int, subjectNumber: Int): Int {
    var loopSize = 1
    while (transform(subjectNumber, loopSize) != key)
        loopSize++
    return loopSize
}

fun main() {
    val subjectNumber = 7
//    val cardPublicKey = 5764801
//    val doorPublicKey = 17807724
    val cardPublicKey = 8421034
    val doorPublicKey = 15993936

    val cardLoopSize = findLoopSizeFor(cardPublicKey, subjectNumber)
    val doorLoopSize = findLoopSizeFor(doorPublicKey, subjectNumber)

    val doorEncriptionKey = transform(doorPublicKey, cardLoopSize)
    val cardEncriptionKey = transform(cardPublicKey, doorLoopSize)
    println("Day 25 A answer is $doorEncriptionKey / $cardEncriptionKey")
}