package aoc2020

import java.io.File
import java.lang.IllegalArgumentException

data class Coord(
    val x: Int = 0,
    val y: Int = 0,
    val z: Int = 0,
)

fun extractGridDirections(line: String) =
    Regex("nw|sw|ne|se|w|e").findAll(line).mapNotNull { it.groupValues[0] }.toList()

fun move(coord: Coord, direction: String) =
    when (direction) {
        "w" -> coord.copy(x = coord.x - 1, y = coord.y + 1)
        "nw" -> coord.copy(y = coord.y + 1, z = coord.z - 1)
        "ne" -> coord.copy(x = coord.x + 1, z = coord.z - 1)
        "e" -> coord.copy(x = coord.x + 1, y = coord.y - 1)
        "se" -> coord.copy(y = coord.y - 1, z = coord.z + 1)
        "sw" -> coord.copy(x = coord.x - 1, z = coord.z + 1)
        else -> throw IllegalArgumentException("invalid direction '$direction'")
    }

fun move(directions: List<String>) =
    directions.fold(Coord()) { coord, direction -> move(coord, direction) }

fun adjacentTiles(coord: Coord) = listOf(
    move(coord, "w"),
    move(coord, "nw"),
    move(coord, "ne"),
    move(coord, "e"),
    move(coord, "se"),
    move(coord, "sw"),
)

fun countBlackAdjacentTiles(blackTiles: Set<Coord>, coord: Coord) =
    adjacentTiles(coord).sumOf { (if (blackTiles.contains(it)) 1 else 0).toInt() }

fun tileNeedsToBeFlipped(blackTiles: Set<Coord>, coord: Coord): Boolean {
    countBlackAdjacentTiles(blackTiles, coord).let { count ->
        if (blackTiles.contains(coord)) {
            if (count == 0 || count > 2)
                return true
        } else {
            if (count == 2)
                return true
        }
    }
    return false
}

fun flip(blackTiles: MutableSet<Coord>, coord: Coord) {
    if (blackTiles.contains(coord))
        blackTiles.remove(coord)
    else
        blackTiles.add(coord)
}

fun flipOneDay(blackTiles: MutableSet<Coord>) {
    blackTiles
        .flatMap { adjacentTiles(it) + it }
        .toSet()
        .filter { tileNeedsToBeFlipped(blackTiles, it) }
        .forEach { flip(blackTiles, it) }
}

fun main() {
    val coordinates = File("input/day24.txt")
        .useLines { it.toList() }
        .map { extractGridDirections(it) }
        .map { move(it) }

    val blackTiles = mutableSetOf<Coord>()
    coordinates.forEach { flip(blackTiles, it) }
    val day24A = blackTiles.count()
    println("Day 24 A answer is $day24A")

    (1..100).forEach {
        flipOneDay(blackTiles)
    }
    val day24B = blackTiles.count()
    println("Day 24 B answer is $day24B")
}