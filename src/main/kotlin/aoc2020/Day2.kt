package aoc2020

import java.io.File

fun main() {
    val passwords = File("input/day2.txt")
        .useLines { it.toList() }
        .filter { it.isNotBlank() }
        .map { extractPassword(it) }
        .filterNotNull()

    println("Day 2 A answer is ${passwords.count { validatePasswordCount(it) }}")
    println("Day 2 B answer is ${passwords.count { validatePasswordPositions(it) }}")
}