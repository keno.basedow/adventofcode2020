package aoc2020

import java.io.File

fun isPassportValid(passport: Map<String, String>): Boolean {
    if ((passport["byr"]?.toIntOrNull() ?: 0) !in 1920..2002) return false
    if ((passport["iyr"]?.toIntOrNull() ?: 0) !in 2010..2020) return false
    if ((passport["eyr"]?.toIntOrNull() ?: 0) !in 2020..2030) return false

    val heightMatch = Regex("""(\d+)(cm|in)""").matchEntire(passport.getOrDefault("hgt", "")) ?: return false
    when (heightMatch.groups[2]?.value) {
        "cm" -> if (heightMatch.groups[1]?.value?.toIntOrNull() !in 150..193) return false
        "in" -> if (heightMatch.groups[1]?.value?.toIntOrNull() !in 59..76) return false
        else -> return false
    }

    Regex("""#[0-9a-f]{6}""").matchEntire(passport.getOrDefault("hcl", "")) ?: return false
    if (passport["ecl"] !in arrayOf("amb", "blu", "brn", "gry", "grn", "hzl", "oth")) return false
    Regex("""\d{9}""").matchEntire(passport.getOrDefault("pid", "")) ?: return false

    return true
}

fun main() {
    val fields = listOf(
        "byr",
        "iyr",
        "eyr",
        "hgt",
        "hcl",
        "ecl",
        "pid",
    )
    val passports = File("input/day4.txt").readText()
        .split(Regex("""\r?\n\s*\r?\n"""))
        .filter { it.isNotBlank() }
        .map { s ->
            s.split(Regex("""\s+"""))
                .filter { it.isNotBlank() }
                .map { it.split(':', limit = 2) }
                .associate { it[0] to it[1] }
        }

    val validAPassportCount = passports.count { it.keys.containsAll(fields) }
    println("Day 4 A answer is $validAPassportCount")

    val validBPassportCount = passports.count { isPassportValid(it) }
    println("Day 4 B answer is $validBPassportCount")
}