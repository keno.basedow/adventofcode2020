package aoc2020

import java.lang.IllegalArgumentException

fun maskValue(value: Long, mask: String): Long {
    val oneMask = mask.replace('X', '0').toLong(2)
    val zeroMask = mask.replace('X', '1').toLong(2)
    return value or oneMask and zeroMask
}

fun maskAddress(address: Long, mask: String): List<Long> {
    var result = listOf(address)
    for ((i, c) in mask.withIndex()) {
        val charMask = 1L shl (mask.length - 1 - i)
        when (c) {
            'X' -> result = result.flatMap { listOf(it and charMask.inv(), it or charMask) }
            '0' -> continue
            '1' -> result = result.map { it or charMask }
            else -> throw IllegalArgumentException("invalid mask character")
        }
    }
    return result
}

fun extractMask(line: String) =
    Regex("""mask\s*=\s*([X01]{36})""")
        .matchEntire(line)?.groups?.elementAt(1)?.value

data class Write(val address: Long, val value: Long)
fun extractWrite(line: String): Write? {
    val match = Regex("""mem\[(\d+)\]\s+=\s+(\d+)""").matchEntire(line)
    match?.groups?.elementAt(1)?.value?.toLongOrNull()?.let { address ->
        match?.groups?.elementAt(2)?.value?.toLongOrNull()?.let { value ->
            return Write(address, value)
        }
    }
    return null
}

fun runMaskValue(lines: List<String>): Long {
    var mask = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
    val memory = mutableMapOf<Long, Long>()
    for (line in lines) {
        extractMask(line)?.let {
            mask = it
        }
        extractWrite(line)?.let {
            memory[it.address] = maskValue(it.value, mask)
        }
    }
    return memory.values.sumOf { it }
}

fun runMaskAddress(lines: List<String>): Long {
    var mask = "000000000000000000000000000000000000"
    val memory = mutableMapOf<Long, Long>()
    for (line in lines) {
        extractMask(line)?.let {
            mask = it
        }
        extractWrite(line)?.let { write ->
            maskAddress(write.address, mask).forEach {
                memory[it] = write.value
            }
        }
    }
    return memory.values.sumOf { it }
}
