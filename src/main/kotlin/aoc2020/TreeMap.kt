package aoc2020

class TreeMap(private val map: List<List<Boolean>>) {

    val width = map.first().size
    val height = map.size

    fun isTree(x: Int, y: Int) = map[y][x % width]

    fun countTrees(xx: Int, yy: Int): Int {
        var count = 0
        var x = xx
        var y = yy
        while (y < height) {
            count += if (isTree(x, y)) 1 else 0
            x += xx
            y += yy
        }
        return count
    }
}