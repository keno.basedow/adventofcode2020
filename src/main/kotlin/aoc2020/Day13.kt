package aoc2020

import java.io.File

data class IndexedBus(val index: Int, val bus: Long)

fun main() {
    val lines = File("input/day13.txt")
        .useLines { it.toList() }

    val estimatedDeparture = extractDepature(lines[0])
    val busDepartures = extractBusDepatures(lines[1])

    val waitingTimes = busDepartures.map { it to minutesUntilBusDeparture(it, estimatedDeparture) }
    val busWithMinWaitingTime = waitingTimes.minByOrNull { b -> b.second } ?: 0 to 0
    println("Day 13 A answer is ${busWithMinWaitingTime.first * busWithMinWaitingTime.second}")

    val indexedBusses: List<IndexedBus> = lines[1]
        .split(",")
        .mapIndexedNotNull { index, s -> if (s == "x") null else IndexedBus(index, s.toLong()) }

    var stepSize = indexedBusses.first().bus
    var time = 0L
    indexedBusses.drop(1).forEach { (offset, bus) ->
        while ((time + offset) % bus != 0L) {
            time += stepSize
        }
        stepSize *= bus // New Ratio!
    }
    println("Day 13 B answer is $time")
}