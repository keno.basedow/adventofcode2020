package aoc2020

import java.io.File

fun main() {
    val program = File("input/day8.txt")
        .useLines { it.toList() }
        .map { parseInstruction(it) }

    val execution = run(program)
    println("Day 8 A answer is ${execution.accumulator}")

    for (i in program.indices) {
        val newProgram = when (program[i].command) {
            ::nop -> program.mapIndexed { j, instruction -> if (i == j) instruction.copy(command = ::jmp) else instruction }
            ::jmp -> program.mapIndexed { j, instruction -> if (i == j) instruction.copy(command = ::nop) else instruction }
            else -> continue
        }
        val newExecution = run(newProgram)
        if (!newExecution.infiniteLoop)
            println("Day 8 B answer is ${newExecution.accumulator}")
    }
}