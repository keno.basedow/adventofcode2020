package aoc2020

const val REPORT_SUM = 2020

tailrec fun expense(report: List<Int>, reportSum: Int = REPORT_SUM): Int {
    if (report.size < 2)
        return 0

    val one = report[0]
    val two = reportSum - one
    if (two > 0 && report.contains(two))
        return one * two

    return expense(report.drop(1), reportSum)
}

tailrec fun expense3(report: List<Int>): Int {
    if (report.size < 3)
        return 0

    val one = report[0]
    val newList = report.drop(1)

    val expense2 = expense(newList, REPORT_SUM - one)
    if (expense2 > 0)
        return one * expense2

    return expense3(newList)
}