package aoc2020

fun recite(startSequence: List<Int>, endIndex: Int): Int {
    val occurrence = startSequence.dropLast(1).mapIndexed { index, number -> number to index }.toMap().toMutableMap()
    var index = startSequence.size - 1
    var last = startSequence.last()
    var next = 0
    while (index < (endIndex - 1)) {
        if (occurrence.contains(last)) {
            next = index - occurrence.getOrDefault(last, 0)
        } else {
            next = 0
        }
        occurrence[last] = index++
        last = next
    }
    return last
}