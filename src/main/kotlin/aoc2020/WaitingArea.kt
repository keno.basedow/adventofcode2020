package aoc2020


fun printLayout(layout: List<String>) {
    println()
    layout.forEach { println(it) }
}

data class XY(val x: Int, val y: Int)

val adjacent = listOf(
    XY(-1, -1),
    XY(0, -1),
    XY(1, -1),
    XY(-1, 0),
    XY(1, 0),
    XY(-1, 1),
    XY(0, 1),
    XY(1, 1),
)

fun tile(layout: List<String>, x: Int, y: Int) =
    layout.elementAtOrNull(y)?.elementAtOrNull(x)

fun countAdjacentOccupied(layout: List<String>, x: Int, y: Int) = adjacent
    .map { XY(it.x + x, it.y + y) }
    .count { tile(layout, it.x, it.y)  == '#' }

fun lineOfSightOccupied(layout: List<String>, x: Int, y: Int, direction: XY): Int {
    var xx = x
    var yy = y
    while (yy in layout.indices && xx in layout[yy].indices) {
        xx += direction.x
        yy += direction.y
        if (tile(layout, xx, yy) == '#') return 1
        if (tile(layout, xx, yy) == 'L') return 0
    }
    return 0
}

fun countLineOfSightsOccupied(layout: List<String>, x: Int, y: Int) = adjacent
    .sumOf { lineOfSightOccupied(layout, x, y, it) }

fun modifiedSeat(layout: List<String>, x: Int, y: Int, counter: (List<String>, Int, Int) -> Int, leaveOccupiedCount: Int) = when (layout[y][x]) {
    'L' -> if (counter(layout, x, y) == 0) '#' else 'L'
    '#' -> if (counter(layout, x, y) >= leaveOccupiedCount) 'L' else '#'
    else -> '.'
}

fun modifiedLayout(layout: List<String>, counter: (List<String>, Int, Int) -> Int, leaveOccupiedCount: Int) =
    layout.mapIndexed { y, row ->
        row.mapIndexed() { x, _ ->
            modifiedSeat(layout, x, y, counter, leaveOccupiedCount)
        }.joinToString("")
    }

fun modifiedLayoutUntilStable(layout: List<String>, counter: (List<String>, Int, Int) -> Int, leaveOccupiedCount: Int): List<String> {
    var prevLayout = layout
    var newLayout = layout
    do {
        prevLayout = newLayout
        newLayout = modifiedLayout(prevLayout, counter, leaveOccupiedCount)
    } while (newLayout != prevLayout)
    return newLayout
}

fun countSeatsOccupied(layout: List<String>) =
    layout.sumOf { row -> row.count { it == '#' } }
