package aoc2020

import java.io.File

fun main() {
    val layout = File("input/day11.txt")
        .useLines { it.toList() }

    println("Day 11 A answer is ${countSeatsOccupied(modifiedLayoutUntilStable(layout, ::countAdjacentOccupied, 4))}")
    println("Day 11 B answer is ${countSeatsOccupied(modifiedLayoutUntilStable(layout, ::countLineOfSightsOccupied, 5))}")
}