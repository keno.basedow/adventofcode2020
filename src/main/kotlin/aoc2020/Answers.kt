package aoc2020

fun countAnyone(answers: List<String>) =
    answers.map { it.toSet() }
        .reduce { sum, a -> sum + a }
        .count()

fun countEveryone(answers: List<String>): Int {
    val group = answers.map { it.toSet() }
        .reduce { sum, a -> sum.intersect(a) }
    return group.count()
}


fun answersSum(answersList: List<List<String>>, counter: (List<String>) -> Int) =
    answersList.sumOf { counter(it) }