package aoc2020

import java.io.File
import kotlin.math.abs

fun main() {
    val lines = File("input/day12.txt")
        .useLines { it.toList() }

    val actions = lines.map { extractAction(it) }
    val ship = actions.fold(Ship()) { ship, a -> a.command(ship, a.value) }
    println("Day 12 A answer is ${abs(ship.ns) + abs(ship.ew)}")

    val wpActions = lines.map { extractWaypointAction(it) }
    val wpShip = wpActions.fold(Ship()) { ship, a -> a.command(ship, a.value) }
    println("Day 12 B answer is ${abs(wpShip.ns) + abs(wpShip.ew)}")
}