package aoc2020

import java.lang.IllegalArgumentException

data class Instruction(
    val command: (Execution, Int) -> Execution,
    val value: Int
)

data class Execution(
    val accumulator: Int = 0,
    val position: Int = 0,
    val history: List<Int> = emptyList(),
    val infiniteLoop: Boolean = false
)

fun nop(execution: Execution, value: Int) =
    execution.copy(
        position = execution.position + 1,
        history = execution.history + execution.position
    )

fun acc(execution: Execution, value: Int) =
    execution.copy(
        accumulator = execution.accumulator + value,
        position = execution.position + 1,
        history = execution.history + execution.position
    )

fun jmp(execution: Execution, value: Int) =
    execution.copy(
        position = execution.position + value,
        history = execution.history + execution.position
    )

fun commandByName(name: String) = when(name) {
    "nop" -> ::nop
    "acc" -> ::acc
    "jmp" -> ::jmp
    else -> throw IllegalArgumentException("Unknown command $name")
}

fun parseInstruction(line: String): Instruction {
    with(line.split(' ')) {
        return Instruction(commandByName(get(0)), get(1).toInt())
    }
}

fun run(program: List<Instruction>): Execution {
    var execution = Execution()
    while (execution.position < program.size) {
        if (execution.position in execution.history) {
            return execution.copy(infiniteLoop = true)
        }
        with(program[execution.position]) {
            execution = command(execution, value)
        }
    }
    return execution
}