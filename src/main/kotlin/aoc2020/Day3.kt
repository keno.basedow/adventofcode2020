package aoc2020

import java.io.File

fun main() {
    val map = File("input/day3.txt")
        .useLines { it.toList() }
        .filter { it.isNotBlank() }
        .map { it.map { c -> c == '#' } }

    val treeMap = TreeMap(map)

    val answerA = treeMap.countTrees(3, 1)
    println("Day 3 A answer is $answerA")

    val answerB =
        treeMap.countTrees(1, 1) *
        treeMap.countTrees(3, 1) *
        treeMap.countTrees(5, 1) *
        treeMap.countTrees(7, 1) *
        treeMap.countTrees(1, 2)
    println("Day 3 B answer is $answerB")
}