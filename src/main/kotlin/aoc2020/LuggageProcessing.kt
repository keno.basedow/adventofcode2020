package aoc2020

fun extractLuggageInfo(rule: String): Pair<String, Map<String, Int>>?  {
    val containMatch = Regex("""([\w\s]+)\s+bags?\s+contain\s+([^.]*)\.?""").matchEntire(rule) ?: return null
    val containingBag = containMatch.groups[1]?.value ?: return null
    val containedRule = containMatch.groups[2]?.value ?: return null
    val containedBags = containedRule.split(Regex("""\s*,\s*"""))
        .map { r ->
            Regex("""(\d+)\s+([\w\s]+)\s+bags?""").matchEntire(r)?.groups?.let { group ->
                group[1]?.value?.toIntOrNull()?.let { no -> group[2]?.value?.let { name -> name to no } }
            }
        }
        .filterNotNull()
        .toMap()
    return Pair(containingBag, containedBags)
}

fun getContainingBags(bag: Bag): Set<String> {
    if (bag.containedIn.isEmpty()) return setOf(bag.name)
    return setOf(bag.name) + bag.containedIn.fold(emptySet()) { acc, b -> acc + getContainingBags(b)}
}

fun calculateBagCount(bagInfo: Map<String, Map<String, Int>>, bagName: String): Int {
    val bags = bagInfo.getOrDefault(bagName, emptyMap())
    return bags.entries.fold(0) { acc, e -> acc + e.value + e.value * calculateBagCount(bagInfo, e.key) }
}