package aoc2020

fun main() {
    println("Day 15 A answer is ${recite(listOf(16,11,15,0,1,7), 2020)}")
    println("Day 15 B answer is ${recite(listOf(16,11,15,0,1,7), 30000000)}")
}