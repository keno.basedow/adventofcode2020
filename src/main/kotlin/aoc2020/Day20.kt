package aoc2020

import java.io.File
import java.lang.IllegalArgumentException

data class Tile(
    val id: Long,
    val grid: List<String>,
) {
    val topBorder: String
        get() = grid.first()
    val rightBorder: String
        get() = grid.map { it.last() }.joinToString("")
    val bottomBorder
        get() = grid.last()
    val leftBorder
        get() = grid.map { it.first() }.joinToString("")

    private fun flipped() = copy(grid = grid.reversed())

    private fun rotated(count: Int = 1): Tile {
        var newGrid = grid
        (1..count).forEach { _ ->
            newGrid = newGrid.indices.map { index ->
                newGrid.map { it[index] }.reversed().joinToString("")
            }
        }
        return copy(grid = newGrid)
    }

    fun variations() =
        (1..4).map {
            rotated(it)
        } + (1..4).map {
            flipped().rotated(it)
        }

    fun withoutBorders() = copy(grid = grid.drop(1).dropLast(1).map { it.drop(1).dropLast(1) })

    fun print() {
        println("Tile $id:")
        grid.forEach {
            println(it)
        }
        println()
    }

    private fun findPattern(pattern: List<String>, x: Int, y: Int): Boolean {
        for (yy in pattern.indices) {
            val p = pattern[yy]
            val g = grid[y + yy]
            for (xx in p.indices) {
                if (p[xx] != '#') continue
                if (g[x + xx] != '#') return false
            }
        }
        return true
    }

    fun findPattern(pattern: List<String>): Int {
        var count = 0
        for (y in 0..(grid.size - pattern.size)) {
            for (x in 0..(grid.first().length - pattern.first().length)) {
                count += if (findPattern(pattern, x, y)) 1 else 0
            }
        }
        return count
    }
}

fun findLeftTile(tiles: List<Tile>, tile: Tile) =
    tiles.mapNotNull { t -> t.variations().singleOrNull { tile.id != it.id && tile.leftBorder == it.rightBorder } }.singleOrNull()

fun findTopTile(tiles: List<Tile>, tile: Tile) =
    tiles.mapNotNull { t -> t.variations().singleOrNull { tile.id != it.id && tile.topBorder == it.bottomBorder } }.singleOrNull()

fun findRightTile(tiles: List<Tile>, tile: Tile) =
    tiles.mapNotNull { t -> t.variations().singleOrNull { tile.id != it.id && tile.rightBorder == it.leftBorder } }.singleOrNull()

fun findBottomTile(tiles: List<Tile>, tile: Tile) =
    tiles.mapNotNull { t -> t.variations().singleOrNull { tile.id != it.id && tile.bottomBorder == it.topBorder } }.singleOrNull()

fun tileRow(tiles: List<Tile>, tile: Tile): List<Tile> {
    val next = findRightTile(tiles, tile) ?: return listOf(tile)
    return listOf(tile) + tileRow(tiles, next)
}

fun tileTable(tiles: List<Tile>, tile: Tile): List<List<Tile>> {
    val next = findBottomTile(tiles, tile) ?: return listOf(tileRow(tiles, tile))
    return listOf(tileRow(tiles, tile)) + tileTable(tiles, next)
}

fun mergeRow(row: List<Tile>) =
    Tile(id = 0, grid = row.first().grid.indices.map { index ->
        row.fold("") { acc, tile ->
            acc + tile.grid[index]
        }
    })

fun mergeTable(table: List<List<Tile>>) =
    Tile(id = 0, grid = table.fold(emptyList<String>()) { acc, row ->
        acc + mergeRow(row).grid
    })

fun printTileRow(row: List<Tile>) {
    for (index in row.first().grid.indices) {
        for (tile in row) {
            print(tile.grid[index])
        }
        println()
    }
}

fun countHashes(grid: List<String>) =
    grid.sumOf { line -> line.count { it == '#' } }

fun main() {
    val tiles = File("input/day20.txt").readText()
        .split(Regex("""\r?\n\s*\r?\n"""))
        .filter { it.isNotBlank() }
        .map { tileLines -> tileLines.split(Regex("""\r?\n""")).filter { it.isNotBlank() } }
        .map {
            Tile((Regex("""Tile (\d+):""").matchEntire(it[0])?.groupValues?.getOrNull(1)?.toLongOrNull()
                ?: throw IllegalArgumentException("tile id missing")), it.drop(1))
        }

    val cornerTiles = tiles.mapNotNull { tile ->
        tile.variations().firstOrNull { findLeftTile(tiles, it) == null && findTopTile(tiles, it) == null }
    }

    val day20A = cornerTiles
        .map { it.id }
        .reduce { acc, id -> acc * id }
    println("Day 20 A answer is $day20A")

    val startTile = cornerTiles.first()
    val table = tileTable(tiles, startTile)
    val noBorders = table.map { row ->
        row.map { it.withoutBorders() }
    }
    val mapTile = mergeTable(noBorders)

    val seaMonster = listOf(
        "                  # ",
        "#    ##    ##    ###",
        " #  #  #  #  #  #   ",
    )
    val seaMonsterCount = mapTile.variations()
        .map { it.findPattern(seaMonster)}
        .single { it > 0 }
    val day20B = countHashes(mapTile.grid) - (seaMonsterCount * countHashes(seaMonster))
    println("Day 20 B answer is $day20B")
}