package aoc2020

import java.io.File

fun main() {
    val lines = File("input/day14.txt")
        .useLines { it.toList() }

    println("Day 14 A answer is ${runMaskValue(lines)}")
    println("Day 14 B answer is ${runMaskAddress(lines)}")
}