package aoc2020

fun numberValid(number: Long, preamble: List<Long>): Boolean {
    for (i in 0 until (preamble.size - 1)) {
        for (j in (i + 1) until preamble.size) {
            if (preamble[i] + preamble[j] == number)
                return true
        }
    }
    return false
}