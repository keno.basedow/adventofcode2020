package aoc2020

import java.io.File
import java.lang.IllegalArgumentException

fun main() {
    val parts = File("input/day19.txt").readText()
        .split(Regex("""\r?\n\s*\r?\n"""))
        .filter { it.isNotBlank() }

    val rules = parts[0]
        .split(Regex("""\r?\n"""))
        .filter { it.isNotBlank() }
        .mapNotNull { line ->
            Regex("""(\d+):\s*(.+)""").matchEntire(line)?.let {
                it.groupValues[1].toInt() to it.groupValues[2]
            }
        }.toMap()

    val messages = parts[1]
        .split(Regex("""\r?\n"""))
        .filter { it.isNotBlank() }

    val reRules = Regex(extractMessageRule(rules))
    val day19A = messages.filter { reRules.matchEntire(it) != null }.size
    println("Day 19 A answer is $day19A")

    val day19B = messages.filter { matchesRecursive(it,
        extractMessageRule(rules, 42),
        extractMessageRule(rules, 31),
        5) }.size
    println("Day 19 B answer is $day19B")
}

fun matchesRecursive(message: String, rule42: String, rule31: String, depth: Int): Boolean {
    // (42 | 42 42 | 42 42 42 ...) (42 31 | 42 42 31 31 | 42 42 42 31 31 31 ...)
    for (repeat8 in 1..depth) {
        for (repeat11 in 1..depth) {
            val reRule = Regex("${rule42.repeat(repeat8)}${rule42.repeat(repeat11)}${rule31.repeat(repeat11)}")
            if (reRule.matchEntire(message) != null) return true
        }
    }
    return false
}

fun extractMessageRule(rules: Map<Int, String>, ruleNumber: Int = 0): String {
    val rule = rules[ruleNumber] ?: throw IllegalArgumentException("rule $ruleNumber not found")
    extractCharRule(rule)?.let { return it }
    Regex("""([^|]+)\|([^|]+)""").matchEntire(rule)?.let {
        return "(${extractListRule(rules, it.groupValues[1])}|${extractListRule(rules, it.groupValues[2])})"
    }
    extractListRule(rules, rule).let { return it }
}

fun extractListRule(rules: Map<Int, String>, rule: String) =
    Regex("""\s+""").split(rule)
        .filter { it.isNotBlank() }
        .fold("") { acc, number ->
            acc + extractMessageRule(rules, number.toInt())
        }

fun extractCharRule(rule: String) = Regex(""""(\w)"""").matchEntire(rule)?.let {
    it.groupValues[1]
}