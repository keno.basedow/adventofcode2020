package aoc2020

import java.lang.IllegalArgumentException

data class Cup(
    val label: Long,
    var next: Cup? = null,
) {
    fun toSequence(ignoreFirst: Boolean = false): String {
        var result = if (ignoreFirst) "" else label.toString()
        var cup = next()
        while (cup != this) {
            result += cup.label.toString()
            cup = cup.next()
        }
        return result
    }

    fun next() = next ?: throw IllegalArgumentException("cups circle is no full loop")
    fun next(count: Int) = (1..count).fold(this) { cup, _ -> cup.next() }

    fun find(label: Long) = cupsByLabel[label]

    fun play(): Cup {
        val removed = next()
        next = removed.next(3)
        val removedLabels = listOf(removed.label, removed.next().label, removed.next().next().label)

        var destinationLabel = label
        do {
            destinationLabel--
            if (destinationLabel < 1) destinationLabel = max
        } while (destinationLabel in removedLabels)
        val destination = find(destinationLabel) ?: throw IllegalArgumentException("destination label $destinationLabel not found")
        val destinationEnd = destination.next()

        destination.next = removed
        removed.next(2).next = destinationEnd

        return next()
    }

    fun playMany(count: Long) =
        (1L..count).fold(this) { cup, _ -> cup.play() }

    companion object {
        private val cupsByLabel = mutableMapOf<Long, Cup>()
        private var max = 0L

        fun generateCircle(sequence: String, max: Long = sequence.length.toLong()): Cup {
            this.max = max
            cupsByLabel.clear()
            val first = Cup(sequence.first().digitToInt().toLong())
            cupsByLabel[first.label] = first
            var previous: Cup = first
            for (char in sequence.drop(1)) {
                val cup = Cup(char.digitToInt().toLong())
                cupsByLabel[cup.label] = cup
                previous.next = cup
                previous = cup
            }
            for (label in sequence.length+1..max) {
                val cup = Cup(label)
                cupsByLabel[cup.label] = cup
                previous.next = cup
                previous = cup
            }
            previous.next = first
            return first
        }
    }
}

fun main() {
//    val input = "389125467"
    val input = "247819356"

    val startA = Cup.generateCircle(input)
    val endA = startA.playMany(100)
    val day23A = endA.find(1)?.toSequence(ignoreFirst = true)
    println("Day 23 A answer is $day23A")

    val startB = Cup.generateCircle(input, 1_000_000L)
    val endB = startB.playMany(10_000_000L)
    val oneB = endB.find(1L)
    val day23B = (oneB?.next()?.label ?: 0L) * (oneB?.next(2)?.label ?: 0L)
    println("Day 23 B answer is $day23B")
}