package aoc2020

import java.io.File

fun main() {
    val seatIds = File("input/day5.txt")
        .useLines { it.toList() }
        .filter { it.isNotBlank() }
        .map { calculateSeatId(it) }
        .toSet()

    val highestSeatId = seatIds.maxOrNull() ?: 0
    println("Day 5 A answer is $highestSeatId")

    val emptySeatIds = (1..highestSeatId).toSet() - seatIds
    println("Day 5 B answer is ${emptySeatIds.last()}")
}