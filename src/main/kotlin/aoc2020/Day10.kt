package aoc2020

import java.io.File

val cache = mutableMapOf<Int, Long>()

fun getNumberOfPaths(number: Int, list: List<Int>): Long {
    if (number in cache) return cache.getOrDefault(number, 0)

    val fitting = list.filter { it in number+1..number+3 }
    if (fitting.isEmpty()) {
        cache[number] = 1L
//        println("$number: 1")
        return 1L
    }

    val result = fitting.fold(0L) { acc, n -> acc + getNumberOfPaths(n, list) }
    cache[number] = result
//    println("$number: $result")
    return result
}

fun main() {
    val adapters = File("input/day10.txt")
        .useLines { it.toList() }
        .mapNotNull { it.toIntOrNull() }
        .plus(0)
        .run { plus((maxOrNull() ?: 0) + 3) }
        .sorted()
    val differences = adapters
        .zipWithNext()
        .map { it.second - it.first }

    val diff1Count = differences.count { it == 1 }
    val diff3Count = differences.count { it == 3 }
    println("Day 10 A answer is ${diff1Count * diff3Count}")

//    println(adapters)
//    println(differences)
    println("Day 10 B answer is ${getNumberOfPaths(0, adapters)}")
}