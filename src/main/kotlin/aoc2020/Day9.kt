package aoc2020

import java.io.File

fun main() {
    val numbers = File("input/day9.txt")
        .useLines { it.toList() }
        .map { it.toLong() }
    val preambleSize = 25

    var weakNumber = 0L
    for (i in preambleSize until numbers.size) {
        if (!numberValid(numbers[i], numbers.slice((i - preambleSize) until i))) {
            weakNumber = numbers[i]
            break
        }
    }
    println("Day 9 A answer is $weakNumber")

    for (i in numbers.indices) {
        var sum = 0L
        var min = numbers[i]
        var max = numbers[i]
        for (j in i..numbers.size) {
            sum += numbers[j]
            min = if (numbers[j] < min) numbers[j] else min
            max = if (numbers[j] > max) numbers[j] else max
            if (sum == weakNumber) {
                println("Day 9 B answer is ${min + max}")
                return
            } else if (sum > weakNumber) {
                break
            }
        }
    }
}